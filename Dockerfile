FROM gitlab-registry.cern.ch/acc-co/devops/docker/cbng-images:latest-jdk8

RUN yum -y install \
which \
git \
phonebook \
&& yum -y clean all && rm -fr /var/cache

CMD echo "Container is up!"

