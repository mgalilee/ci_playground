pipeline {
	agent {
        dockerfile { 
            dir '.'
        }
	}

	options {
		buildDiscarder logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '', numToKeepStr: '32')

		disableConcurrentBuilds()
	}

	triggers {
  		pollSCM 'H/2 * * * *'
	}

	stages {
		stage('Prepare') {
			steps {
				dir('nope') {
					checkout([$class: 'SubversionSCM', additionalCredentials: [], excludedCommitMessages: '', excludedRegions: '', excludedRevprop: '', excludedUsers: '', filterChangelog: false, ignoreDirPropChanges: false, includedRegions: '', locations: [[cancelProcessOnExternalsFail: true, credentialsId: 'df213beb-0d4e-4d4b-97c2-46e43e578274', depthOption: 'infinity', ignoreExternalsOption: true, local: '.', remote: 'svn+ssh://acccosvn.cern.ch/acc-fesa/branches/fesa-class/DQAmx/LS2_refactoring/playground']], quietOperation: true, workspaceUpdater: [$class: 'UpdateUpdater']])
				}
				sh '''
					exit 0
				'''
			}
		}
	}

	post {
		failure {
			script {
				echo responsibles()
			}
			mattermostSend color: 'danger',
			    text: "#### #${env.JOB_NAME} \n <${env.BUILD_URL}|build #${env.BUILD_ID}>, executed on ${env.NODE_NAME} - ${responsibles()}, please have a look",
			    message: "Failure! :weary:"
		}
		fixed {
			mattermostSend color: 'good',
			    text: "#### #${env.JOB_NAME} \n <${env.BUILD_URL}|build #${env.BUILD_ID}>, executed on ${env.NODE_NAME}",
			    message: "Fixed! :sweat_smile:"
		}
		cleanup {
			cleanWs()
		}
	}
}

def responsibles() {
	responsibles = [] as Set
	emails = []
	for (changeSet in currentBuild.changeSets) {
		for (change in changeSet) {
			if (change instanceof hudson.plugins.git.GitChangeSet) {
				emails << change.getAuthorEmail()
			} else if (change instanceof hudson.scm.SubversionChangeLogSet$LogEntry) {
				responsibles << "@" + change.getAuthor().toString()
			} else {
				echo change.getClass().toString()
			}
		}
	}
	// somehow, this for loop cannot be simply merged with the previous one;
	// the 'sh' embeds the step context and attempts to serialize local variables, which fails;
	// hence the intermediary list 'emails'
	for (email in emails) {
		responsible = sh (script: 'phonebook --email ' + email + ' --terse login', returnStdout: true).trim()
		responsibles << "@" + responsible.substring(0, responsible.length() - 1);
	}
	return responsibles.join(" ")
}
